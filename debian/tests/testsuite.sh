#!/bin/bash

cd .. && pymolcas verify standard:000-002,004-006,009-012,014-016,019,023,025,026,028,029 --status
if [ $? != 0 ]; then
    for i in $(basename `cat failed/list` | sed s/.input//); do
        echo "----> $i.err:"
        cat failed/*_$i.err
        echo "----> $i.out:"
        tail -50 failed/*_$i.out
    done
    exit 1
fi
