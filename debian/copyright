Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: openmolcas
Source: https://gitlab.com/Molcas/OpenMolcas

Files: *
Copyright: 1971-2019 Ajitha Devarajan, Alessio Valentini, Alexander Wolf,
 Alexander Zech, Anders Bernhardsson, Anders Ohrn, Andrew M. Sand, Andy May,
 Aron Cohen, Ben Swerts, Bernd Artur Hess, Bernd Schimmelpfennig, Bingbing Suo,
 Bjorn O. Roos, Chad E. Hoyer, Christian Ander, Christian Pomelli, Christophe
 Chipot, Coen de Graaf, Daoling Peng, Denis Jelovina, Dongxia Ma, Eugeniusz
 Bednarz, D. L. Cooper, Felipe Zapata, Felix Plasser, Francesco Aquilante, Gene
 H. Golub, Giovanni Ghigo, Giovanni Li Manni, Grigory A. Shamov, Gunnar
 Karlstrom, Hans-Joachim Werner, H. Rieger, IBM, Ignacio Fdez Galvan, Ignacio
 Fdez. Galvan, Ignacio Fdez. Galván, Ignacio Fdez. Galvan (modified from
 RecPrt), Ignacio Fdez. Galvan (modified from TrcPrt), Ignacio Fdez. Galvan
 (split from gencxctl), Igor Schapiro, Janos G. Angyan, Jeppe Olsen, Jesper
 Norell, Jesper Wisborg Krogh, Joel Creutzberg, Johan Lorentzon, John Burkardt,
 Jonas Bostrom, Jonna Stalring, Jose Manuel Hermida Ramon, Jun-ya Hasegawa,
 Kurt Pfingst, Lasse Kragh Soerensen, Laura Gagliardi, Leon Freitag, Liviu
 Ungur, Luca De Vico, Luis Manuel Frutos, Luis Seijo, Luis Serrano-Andres,
 Manuela Merchan, Marcus Johansson, Margareta R. A. Blomberg, Mark A. Watson,
 Markus P.  Fuelscher, Markus Reiher, Martin Schuetz, Michael A. Saunders,
 Michael Diedenhofen, Mickael G. Delcey, Morgane Vacher, Naoki Nakatani, Nelson
 H. F.  Beebe, Oskar Weser, Par Soderhjelm, Pavel Neogrady, Pawel Salek, Per
 Ake Malmqvist, Per Boussard, Per E. M. Siegbahn, Per-Olof Widmark, Piotr
 Borowski, Quan Phung, Roland Lindh, Samuel Mikes, Sebastian Wouters, Sergey
 Gusarov, Sijia S. Dong, Stefan Knecht, Stefano Battaglia, Steven Vancoillie,
 Takashi Tsuchiya, Ten-no Research Group, Teodoro Laino, Thomas Bondo Pedersen,
 Thomas Dresselhaus, Thorstein Thorsteinsson, T. Thorsteinsson, Valera
 Veryazov, Victor P. Vysotskiy, Walter Gautschi, Yannick Carissan, Yan Zhao,
 Yingjin Ma, Yubin Wang
License: LGPL-2.1
Comment: For the full per-file and per- year copyright information, see
 debian/copyright_full.

Files: debian/*
Copyright: 2019 Michael Banck <mbanck@debian.org>
License: LGPL-2.1

License: LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License version 2.1 as published by the Free Software Foundation.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
Comment: On Debian systems the complete text of the GNU Lesser General Public
 License 2.1 can be found in the file `/usr/share/common-licenses/LGPL-2.1'.
